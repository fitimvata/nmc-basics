<?php

class Database {

    protected $connection;

    public function __construct() {
        $servername = $config['db']['host'];
        $username =   $config['db']['username'];
        $password =   $config['db']['password'];
        $dbname =     $config['db']['dbname'];
        $conn = new mysqli($servername, $username, $password, $dbname);

        if ($conn->connect_error) {
            die("Connection failed: " . $conn->connect_error);
        } 

        $this->connection = $conn;
    }


    public function query($query) {

        $result = $this->connection->query($query);

        if($result) {
            return $result;
        }

        return false;
    }
}
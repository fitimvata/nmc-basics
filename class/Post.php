<?php

class Post extends Database {

    protected $table_name = 'posts';

    public function find($id)
    {
        $query = sprintf(
            "SELECT * FROM %s WHERE id='%d'", 
            $this->table_name, 
            $id
        );

        $result = $this->query($query);

        if(count($result)) {
            return $result[0];
        } 

        return false;
    }
}
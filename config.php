<?php

return [
    'db' => [
        'host'     => 'localhost',
        'dbname'   => 'basics',
        'username' => 'homestead',
        'password' => 'secret',
    ],

    'site' => [
        'base_url' => 'http://basics.test/newmedia',
        'name' => 'Basics',
    ],
];
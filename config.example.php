<?php

return [
    'db' => [
        'host'     => 'localhost',
        'dbname'   => '',
        'username' => '',
        'password' => '',
    ],

    'site' => [
        'base_url' => 'http://localhost/newmedia',
        'name' => 'Basics',
    ],
];
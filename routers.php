<?php

// Custom 404 Handler
$router->set404(function () {
    header($_SERVER['SERVER_PROTOCOL'] . ' 404 Not Found');
    echo '404, route not found!';
});

$router->get('/', function () use($twig) {
    $data = Auth::user();
    echo $twig->render('index.twig', [ 
        'data' => $data,
    ]);
});
